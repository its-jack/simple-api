package xyz.xcvu.ma2.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ShowEnvInfo {
    @Value("${app-name}")
    private String appName;

    public static final String resPath = ""+System.getProperty("user.dir");

    @Bean
    public void onStart(){
        System.out.println("当前启动环境: "+appName);
        System.out.println("当前项目路径: "+resPath);
    }
}

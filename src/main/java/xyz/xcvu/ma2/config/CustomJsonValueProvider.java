package xyz.xcvu.ma2.config;

import org.springframework.stereotype.Component;
import org.ssssssss.magicapi.core.context.RequestEntity;
import org.ssssssss.magicapi.core.interceptor.ResultProvider;

import java.util.HashMap;

@Component
public class CustomJsonValueProvider implements ResultProvider {
    /**
     *   定义返回结果，默认返回JsonBean
     */
    @Override
    public Object buildResult(RequestEntity requestEntity, int code, String message, Object data) {
        // 如果对分页格式有要求的话，可以对data的类型进行判断，进而返回不同的格式
        long timestamp = System.currentTimeMillis();
        return new HashMap<String,Object>(){
            {
                put("code", code);
                put("msg", message);
                put("message", message);
                put("data", data);
                put("executeTime", timestamp - requestEntity.getRequestTime());
            }
        };
    }
}

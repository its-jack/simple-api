package xyz.xcvu.ma2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
// import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
// @EnableSwagger2
public class Ma2Application {

	public static void main(String[] args) {
		// SpringApplication.run(Ma2Application.class, args);
		SpringApplication springApplication=new SpringApplication(Ma2Application.class);
		springApplication.addListeners((ApplicationListener<ApplicationStartedEvent>) event->{
			System.out.println("SprintBoot Started!");
		});
		springApplication.run(args);
	}

}

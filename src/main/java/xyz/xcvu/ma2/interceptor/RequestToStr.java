package xyz.xcvu.ma2.interceptor;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import org.ssssssss.magicapi.core.interceptor.RequestInterceptor;
import org.ssssssss.magicapi.core.model.ApiInfo;
import org.ssssssss.script.MagicScriptContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * 拦截请求
 */
@Component
@Order(99) //拦截器顺序，可选
public class RequestToStr implements RequestInterceptor {
    /*
     * 当返回对象时，直接将此对象返回到页面，返回null时，继续执行后续操作
     */
    @Override
    public Object preHandle(ApiInfo info, MagicScriptContext context, HttpServletRequest request, HttpServletResponse response) {
        String method = request.getMethod();
        String contentType = request.getHeader("Content-Type");
        String bodyStr = "";
        if(method.equals("POST")){
            if(!contentType.equals("application/x-www-form-urlencoded") && !contentType.equals("application/json")){
                bodyStr = this.getPostAsString(request);
            }
            request.setAttribute("BodyStr", bodyStr);
        }
        return null;
    }
    /**
     * 执行完毕之后执行
     * @param value 即将要返回到页面的值
     * @return 返回到页面的对象,当返回null时执行后续拦截器，否则直接返回该值，不执行后续拦截器
     */
    @Override
    public Object postHandle(ApiInfo info, MagicScriptContext context, Object value, HttpServletRequest request, HttpServletResponse response) {
        // System.out.println("请求后：" + info.getPath());
        // System.out.println("返回结果：" + value);
        // 拦截请求并直接返回结果，不继续后续代码
        // 需要注意的是，拦截器返回的结果不会被包裹一层json值，也不会走ResultProvider
        // return new JsonBean<>(100,"拦截器返回");
        // 放开请求，执行后续代码
        return null;
    }

    public String getPostAsString(HttpServletRequest request){
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;
        InputStream inputStream = null;
        try {
            inputStream = request.getInputStream();
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                char[] charBuffer = new char[128];
                int bytesRead;
                while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                    stringBuilder.append(charBuffer, 0, bytesRead);
                }
            }
        } catch (IOException ignored) {

        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return stringBuilder.toString();
    }
}

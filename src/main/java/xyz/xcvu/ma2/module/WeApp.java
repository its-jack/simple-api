package xyz.xcvu.ma2.module;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.api.impl.WxMaServiceImpl;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.config.impl.WxMaDefaultConfigImpl;
import me.chanjar.weixin.common.error.WxErrorException;
import org.springframework.stereotype.Component;
import org.ssssssss.magicapi.core.annotation.MagicModule;
import org.ssssssss.script.annotation.Comment;

@Component
@MagicModule("weapp")
public class WeApp {

    public WxMaService getMaService(
            @Comment("微信小程序的appid") String appId,
            @Comment("微信小程序的Secret")String secret,
            @Comment("微信小程序消息服务器配置的token")String token,
            @Comment("微信小程序消息服务器配置的EncodingAESKey")String aesKey
    ){
        WxMaService wxService = new WxMaServiceImpl();
        WxMaDefaultConfigImpl config = new WxMaDefaultConfigImpl();
        config.setAppid(appId);
        config.setSecret(secret);
        config.setToken(token);
        config.setAesKey(aesKey);
        config.setMsgDataFormat("JSON");
        wxService.setWxMaConfig(config);
        return wxService;
    }

    public WxMaJscode2SessionResult login(WxMaService wxService, String code) throws WxErrorException{
        return wxService.getUserService().getSessionInfo(code);
    }
}

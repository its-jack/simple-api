package xyz.xcvu.ma2.module;

import org.springframework.stereotype.Component;
import org.ssssssss.magicapi.core.annotation.MagicModule;
import org.ssssssss.script.annotation.Comment;

@Component
@MagicModule("gis")
public class Gis {

    private static final double EarthRadius = 6371000; // 平均半径(单位:m)
    private static final  double EquatorRadius = 6378137; // 赤道半径(单位:m)

    private static double getAcos(Double lat1,Double lng1,Double lat2,Double lng2){
        double radiansAX = Math.toRadians(lng1); // A经弧度
        double radiansAY = Math.toRadians(lat1); // A纬弧度
        double radiansBX = Math.toRadians(lng2); // B经弧度
        double radiansBY = Math.toRadians(lat2); // B纬弧度

        // 公式中“cosβ1cosβ2cos（α1-α2）+sinβ1sinβ2”的部分，得到∠AOB的cos值
        double cos = Math.cos(radiansAY) * Math.cos(radiansBY) * Math.cos(radiansAX - radiansBX)
                + Math.sin(radiansAY) * Math.sin(radiansBY);
        return Math.acos(cos); // 反余弦值
    }

    @Comment("通过地球平均半径计算距离")
    public static int getSphereDistance(
            @Comment("A点经度") Double lat1,
            @Comment("A点纬度") Double lng1,
            @Comment("B点经度") Double lat2,
            @Comment("B点纬度") Double lng2
    ) {
        // 经纬度（角度）转弧度。弧度用作参数，以调用Math.cos和Math.sin
        double acos = getAcos(lat1,lng1,lat2,lng2);
        return (int)(EarthRadius * acos); // 最终结果
    }

    @Comment("通过赤道半径计算距离")
    public static int getEquatorDistance(
            @Comment("A点经度") Double lat1,
            @Comment("A点纬度") Double lng1,
            @Comment("B点经度") Double lat2,
            @Comment("B点纬度") Double lng2
    ) {
        double acos = getAcos(lat1, lng1, lat2, lng2);
        return (int)(EquatorRadius * acos); // 最终结果
    }
}

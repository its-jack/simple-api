package xyz.xcvu.ma2.module;

import org.springframework.stereotype.Component;
import org.ssssssss.magicapi.core.annotation.MagicModule;
import org.ssssssss.script.annotation.Comment;

import javax.servlet.http.HttpServletRequest;

@Component
@MagicModule("req")
public class MyReq {

    @Comment("获取请求体为文本")
    public String getPostBody() {
        HttpServletRequest request = org.ssssssss.magicapi.utils.WebUtils.getRequest().get();
        return request.getAttribute("BodyStr").toString();
    }

    @Comment("获取参数为文本")
    public String getQs() {
        HttpServletRequest request = org.ssssssss.magicapi.utils.WebUtils.getRequest().get();
        try{
            return request.getQueryString();
        } catch (Exception e){
            return "";
        }
    }
}

package xyz.xcvu.ma2.module;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import org.springframework.stereotype.Component;
import org.ssssssss.magicapi.core.annotation.MagicModule;
import org.ssssssss.script.annotation.Comment;

import java.io.InputStream;

@Component
@MagicModule("oss")
public class Oss {
    private String endpoint;
    private String accessKeyId;
    private String accessKeySecret;
    private String bucketName;

    public String upload(
            @Comment("文件输入流") InputStream inputStream,
            @Comment("存储路径") String path
    ){
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        path = path.startsWith("/") ? path.substring(1) : path;
        ossClient.putObject(bucketName, path, inputStream);
        ossClient.shutdown();
        return "/" + path;
    }

    public Oss setEndPoint(String str){
        this.endpoint = str;
        return this;
    }

    public Oss setAccessKeyId(String str){
        this.accessKeyId = str;
        return this;
    }

    public Oss setAccessKeySecret(String str){
        this.accessKeySecret = str;
        return this;
    }

    public Oss setBucketName(String str){
        this.bucketName = str;
        return this;
    }
}

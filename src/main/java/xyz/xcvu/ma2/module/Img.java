package xyz.xcvu.ma2.module;

import com.freewayso.image.combiner.ImageCombiner;
import com.freewayso.image.combiner.enums.OutputFormat;
import com.freewayso.image.combiner.enums.ZoomMode;
import org.springframework.stereotype.Component;
import org.ssssssss.magicapi.core.annotation.MagicModule;
import org.ssssssss.script.annotation.Comment;
import java.awt.Color;

@Component
@MagicModule("img")
public class Img {

    @Comment("设置背景图以及手动指定宽高")
    public ImageCombiner setBgByUrl(
            @Comment("背景图地址") String bgImageUrl,
            @Comment("图片宽度") int width,
            @Comment("图片高度") int height
    ) throws Exception{
        return new ImageCombiner( bgImageUrl, width, height, ZoomMode.Height, OutputFormat.JPG);
    }

    @Comment("设置背景图")
    public ImageCombiner setBgByUrl(
            @Comment("背景图地址") String bgImageUrl
    ) throws Exception{
        return new ImageCombiner( bgImageUrl, OutputFormat.JPG);
    }

}

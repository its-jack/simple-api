package xyz.xcvu.ma2.module;

import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.sms.v20210111.SmsClient;
import com.tencentcloudapi.sms.v20210111.models.PullSmsSendStatusRequest;
import com.tencentcloudapi.sms.v20210111.models.SendSmsRequest;
import com.tencentcloudapi.sms.v20210111.models.SendStatusStatisticsRequest;
import org.springframework.stereotype.Component;
import org.ssssssss.magicapi.core.annotation.MagicModule;
import org.ssssssss.script.annotation.Comment;

@Component
@MagicModule("sms")
public class Sms {

    @Comment("获取client")
    public SmsClient getClient(
            @Comment("云账号ID")String secretId,
            @Comment("云账号密钥")String secretKey
    ){
        Credential cred = new Credential(secretId, secretKey);
        // 实例化一个http选项，可选，没有特殊需求可以跳过
        HttpProfile httpProfile = new HttpProfile();
        // httpProfile.setProxyHost("真实代理ip");
        // httpProfile.setProxyPort(真实代理端口);
        /* SDK默认使用POST方法。
         * 如果你一定要使用GET方法，可以在这里设置。GET方法无法处理一些较大的请求 */
        httpProfile.setReqMethod("POST");
        /* SDK有默认的超时时间，非必要请不要进行调整 */
        httpProfile.setConnTimeout(60);
        /* 指定接入地域域名，默认就近地域接入域名为 sms.tencentcloudapi.com ，也支持指定地域域名访问，例如广州地域的域名为 sms.ap-guangzhou.tencentcloudapi.com */
        httpProfile.setEndpoint("sms.tencentcloudapi.com");
        /* 非必要步骤:
         * 实例化一个客户端配置对象，可以指定超时时间等配置 */
        ClientProfile clientProfile = new ClientProfile();
        /* SDK默认用TC3-HMAC-SHA256进行签名
         * 非必要请不要修改这个字段 */
        clientProfile.setSignMethod("HmacSHA256");
        clientProfile.setHttpProfile(httpProfile);
        return new SmsClient(cred, "ap-guangzhou",clientProfile);
    }

    @Comment("SendSmsResponse res = client.SendSms(req);")
    public SendSmsRequest getSmsReq(
            @Comment("短信应用ID") String sdkAppId,
            @Comment("短信签名内容")String signName,
            @Comment("用户侧上下文信息") String sessionContext,
            @Comment("模板 ID") String templateId,
            @Comment("11位手机号") String phoneNumber,
            @Comment("模板参数") String[] templateParamSet
    ){
        SendSmsRequest req = new SendSmsRequest();
        req.setSmsSdkAppId(sdkAppId);
        req.setSignName(signName);
        /* 国际/港澳台短信 SenderId: 国内短信填空，默认未开通 */
        req.setSenderId("");
        req.setSessionContext(sessionContext);
        /* 短信号码扩展号: 默认未开通 */
        req.setExtendCode("");
        req.setTemplateId(templateId);
        String[] phoneNumberSet = {"+86"+phoneNumber};
        req.setPhoneNumberSet(phoneNumberSet);
        req.setTemplateParamSet(templateParamSet);
        return req;
    }

    @Comment("PullSmsSendStatusResponse res = client.PullSmsSendStatus(req);")
    public PullSmsSendStatusRequest getPullStatusReq(
            @Comment("短信应用ID")String sdkAppId
    ){
        PullSmsSendStatusRequest req = new PullSmsSendStatusRequest();
        req.setSmsSdkAppId(sdkAppId);
        // 设置拉取最大条数，最多100条
        Long limit = 10L;
        req.setLimit(limit);
        return req;
    }

    @Comment("SendStatusStatisticsResponse res = client.SendStatusStatistics(req);")
    public SendStatusStatisticsRequest getSendStatus(
            @Comment("短信应用ID")String sdkAppId,
            @Comment("偏移量") Long offset,
            @Comment("开始时间，yyyymmddhh，精确到小时")String beginTime,
            @Comment("注：EndTime 必须大于 beginTime")String endTime
    ){
        SendStatusStatisticsRequest req = new SendStatusStatisticsRequest();
        req.setSmsSdkAppId(sdkAppId);
        Long limit = 10L;
        req.setLimit(limit);
        /* 偏移量 */
        req.setOffset(offset);
        req.setBeginTime(beginTime);
        req.setEndTime(endTime);
        return req;
    }
}

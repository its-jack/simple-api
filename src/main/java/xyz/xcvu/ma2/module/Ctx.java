package xyz.xcvu.ma2.module;

import org.springframework.stereotype.Component;
import org.ssssssss.magicapi.core.annotation.MagicModule;
import org.ssssssss.script.annotation.Comment;

@Component
@MagicModule("ctx")
public class Ctx {

    @Comment("获取当前请求的token")
    public String getToken(){
        return "token";
    }
}

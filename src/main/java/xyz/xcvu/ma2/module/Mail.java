package xyz.xcvu.ma2.module;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.extra.mail.MailAccount;
import cn.hutool.extra.mail.MailUtil;
import org.springframework.stereotype.Component;
import org.ssssssss.magicapi.core.annotation.MagicModule;
import org.ssssssss.script.annotation.Comment;

@Component
@MagicModule("mail")
public class Mail {

    @Comment("发送简单邮件")
    public MailAccount getAccount(
            String host,
            String from,
            String username,
            String password
    ){
        MailAccount account = new MailAccount();
        account.setHost(host);
        account.setPort(465);
        account.setAuth(true);
        account.setSslEnable(true);
        account.setFrom(from);
        account.setUser(username);
        account.setPass(password);
        return account;
    }

    public void sendSimpleMail(
            MailAccount account, String to, String subject, String content
    ){
        MailUtil.send(account, CollUtil.newArrayList(to), subject, content, false);
    }
}

package xyz.xcvu.ma2.module;

import com.jfinal.template.Engine;
import com.jfinal.template.Template;
import org.springframework.stereotype.Component;
import org.ssssssss.magicapi.core.annotation.MagicModule;
import org.ssssssss.script.annotation.Comment;

@Component
@MagicModule("tpl")
public class Tpl {
    private final Engine engine;

    public Tpl(){
        this.engine = Engine.use();
        this.engine.setDevMode(true);
    }

    @Comment("读取字符串为模板")
    public Template setTplByStr(@Comment("读取字符串到模板")String str){
        return this.engine.getTemplateByString(str);
    }
}

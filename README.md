# Simple-Api

#### 介绍
基于[Magic-Api](https://gitee.com/ssssssss-team/magic-api)的实践与应用，开箱即用。

#### 软件架构
基于Magic-Api 2.0，使用了SpringBoot 2.6.0 ；

#### 安装教程

1.  打开IDEA
2.  导入项目
3.  run

#### 使用说明

1.  请查看Magic-Api文档

#### 参与贡献

1.  请提交Issues


#### 特技

1.  官方网站 [401500.net](https://blog.401500.net)

### 鸣谢（排名不分先后）
- Magic-Api
- Sa-Token
- Erupt
- Hutool
- swagger
- enjoy模板
- weixin-java
- ip2region
- pinyin4j
- image-combiner
- 腾讯云
- 阿里云
- 七牛云
- minio